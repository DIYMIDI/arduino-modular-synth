/*

ADSR envelope module

 Handles gate input and creates ADSR envelope.
 All parameters are controlled by pots.
 
 The circuit:
 * Potentiometers attached to analog inputs 0, 1, 2, and 3
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * Gate input on pin 3
 * Output is PWM on pin 5

 Created by Victor X
 http://victorx.eu
 
 This code is in the public domain.

*/

int aPin = A0;
int dPin = A1;
int sPin = A2;
int rPin = A3;

int gatePin = 3;
int pwmPin = 5;

// in sec - 1000 msec is very close to 1024
int maxDuration = 10;

// moments of rising and falling gate fronts
unsigned long triggered = 0;
unsigned long released = 0;

float A,D,S,R;
float lastValue;

void setup() {
Serial.begin(19200);
}

void loop() {
  A = analogRead(aPin) * maxDuration + 1;
  D = analogRead(dPin) * maxDuration + 1;
  S = analogRead(sPin) / 1024.0f;
  R = analogRead(rPin) * maxDuration + 1;
  
  int gate = digitalRead(gatePin);
  unsigned long now = millis();
  
  if (!triggered && (gate == HIGH)) {
    triggered = now;
    released = 0;
  }
  
  if (triggered && (gate == LOW)) {
    released = now;
    triggered = 0;
  }
  
  analogWrite(pwmPin, envelope(now) * 256);
}

float envelope(unsigned long t) {
    if (triggered) {
        long age = t - triggered;
        if (age < A) {
            return lastValue = age / A;
        } else {
            age -= A;
            if (age < D) {
                return lastValue = 1 - (1 - S) * age / D;
            }
            return lastValue = S;
        }
    }
    else if (released) {
        long age = t - released;
        if (age > R) {
            released = 0;
        } else {
    Serial.println(lastValue);
            return lastValue = S * (1 - age / R);
        }
    }
    return lastValue = 0;
}

