/*
 LFO
 
 All parameters are controlled by pots.
 
 The circuit:
 * Potentiometers attached to analog inputs 0, 1, and 2
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * Output is PWM on pin 5.
 * Use RC to filter the PWM. 
 * For frequencies up to 20Hz, R=160ohm and C=50uF
 *
 * The 1st pot controls period between 50msec and 5sec
 * The second pot controls waveform assimetry (like PWM)
 * The third pot selects the waveform type - sine, saw, triangle, or rectangle
 
 Created 18 Feb 2015
 By Victor X
 
 http://victorx.eu
 
 This code is in the public domain.
 
 */

int pwmPin = 5;

int periodPin = A0;
int paramPin = A1;
int switchPin = A2;

float period, param, type;
unsigned long now, started;

// max period in seconds
const float MAX_PERIOD = 5;
const int DIM = 200;
int sine[DIM];
int rect[DIM];
int triangle[DIM];
int saw[DIM];
int *arrs[] = { sine, rect, triangle, saw };
int HALF = DIM / 2;
int idx;

void setup()  {
  Serial.begin(19200);
  float sinePhaseStep = 2 * M_PI / DIM;
  float sawRate = 255.0f / HALF;
  // prepare wave forms in range 0..255 as expected
  // by analogWrite().
  for (int i = 0; i < DIM; ++i)
  {
    sine[i] = sin(i * sinePhaseStep) * 127 + 127;
    bool up = i < HALF;
    saw[i] = i * sawRate /  2;
    triangle[i] = (up ? i : DIM - i) * sawRate;
    rect[i] = up ? 255 : 0;
  }
  started = millis();
}

void loop()  {
  // the shortest period is 50 msec
  period = analogRead(periodPin) * MAX_PERIOD + 50;
  param = analogRead(paramPin) / 1023.0f;
  type = analogRead(switchPin);
  now = millis();
  int reltime = now - started;
  int cross = param * period;
  if (reltime >= period)
  {
    started = now;
    reltime -= period;
  }

  // apply the parameter to stretch one half of the wave form
  // and squeeze the other.
  if (reltime < cross)
  {
    idx = map(reltime, 0, cross, 0, HALF);
  }
  else
  {
    idx = map(reltime, cross, period, HALF, DIM - 1);
  }

  // max type is 1023; there are four wave forms
  // to select from.
  int *a = arrs[(int)(type / 256)];
  analogWrite(pwmPin, a[idx]);
}
