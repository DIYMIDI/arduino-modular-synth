char vals[3];
int cmd;

int freqPinA = 3;
int freqPinB = 5;
int veloPinA = 9;
int veloPinB = 11;
int gatePinA = 2;
int gatePinB = 4;

void setup() {
  //  Set serial port baud rate: must match the rate specified in NotePlayer.cpp
  Serial.begin(19200);
  pinMode(gatePinA, OUTPUT); 		
  pinMode(gatePinB, OUTPUT); 		
}

void loop() {
  int channel;
  
  if (!Serial.available())
  {
    delay(20);
    return;
  }

  cmd = Serial.read();
  
  switch(cmd)
  {
  case 'R': // all reset
  case 'a': // all notes off
    digitalWrite(gatePinA, LOW); 
    digitalWrite(gatePinB, LOW); 
    break;

  case 'n': // note on
    if (Serial.readBytes(vals, 3) == 3)
    {
      if((vals[0] - 1) & 1)
      {
      }
      else
      {
      }
      synth.noteOn(, vals[1], vals[2]);
    }
    break;

  case 'f': // note off
    if (Serial.readBytes(vals, 3) == 3)
    {
      synth.noteOff(channel = (vals[0] - 1) & 0xF, vals[1]);
    }
    break;

  case 'p': // program change
  case 'b': // pitch bend
  case 'c': // control change
    // all ignored
    break;
  }
} 
