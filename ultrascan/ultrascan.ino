// number of sensors
const int NUM = 3;
int trigPins[NUM] = {13, 13, 13};
int echoPins[NUM] = {9, 10, 11};

void setup() {
  Serial.begin (19200);
  for(int i=0; i<NUM; i++)
  {
    pinMode(trigPins[i], OUTPUT);
    digitalWrite(trigPins[i], LOW);
    pinMode(echoPins[i], INPUT);
  }
}

void loop() {
  long duration;
  for(int i=0; i<NUM; i++)
  {
    digitalWrite(trigPins[i], HIGH);
    delayMicroseconds(50);
    digitalWrite(trigPins[i], LOW);
    duration = pulseIn(echoPins[i], HIGH);
    Serial.print(duration);
    Serial.print(" ");
    delayMicroseconds(5000);
  }
  Serial.println("");
  Serial.flush();
  delay(50);
}
